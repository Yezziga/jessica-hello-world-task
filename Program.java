import java.util.ArrayList;

public class Program {
  public static void main(String[] args) {
    ArrayList<String> list = new ArrayList<>(); // Instansiate an ArrayList & store in list

    // Add names & species of the pets to list
    list.add("Charlie the dog");
    list.add("Gigi the guinea pig");
    list.add("Rosie the hamster");
    list.add("Blue the fish");
    list.add("Pricken the fish");

    System.out.println("List of my pets (current and previous)");
    // Print every string from list
    for(String str : list ) {
      System.out.println(str);
    }
  }
}
