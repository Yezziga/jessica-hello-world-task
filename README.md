# Jessica-hello-world-task

Screenshot of the .java file being compiled to .class:

![](images/compile.PNG)

Screenshot of creating a .jar file of the .class:

![](images/createJar.PNG)

Screenshot of the execution of the .jar file:

![](images/executeJar.PNG)
